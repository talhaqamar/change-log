package com.oliver.project.database;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class DatabaseHelper extends SQLiteOpenHelper {
    //DB name
	private static final String DATABASE_NAME = "project";
	 private static final int DATABASE_VERSION = 1;
	/****************Events table*********************************/
	//Table name
	private static final String PROJECT_TABLE = "project";
	//Column name
	private static final String ID = "id";
	private static final String URL = "url";
	private static final String EMAIL = "email";
	private static final String PASSWORD = "password";
	private static final String DESC = "desc";
	private static final String PIC1 = "pic1";
	private static final String PIC2 = "pic2";
	private static final String PIC3 = "pic3";
	private static final String SERIALNO = "serialno";
	private static final String PARTNO = "partno";
	
	
	/******************Event table end*******************************/
	
	/********Create tables Queries*******************/	
	private final String PROJECT_QUERY = "CREATE TABLE " + PROJECT_TABLE + "("
            + ID + " INTEGER PRIMARY KEY,"+URL + " TEXT," + EMAIL + " TEXT,"+ PASSWORD + " TEXT,"+ DESC + " TEXT,"+ PIC1 + " TEXT,"+ PIC2 + " TEXT,"+ PIC3 + " TEXT,"+ SERIALNO + " TEXT,"+ PARTNO + " TEXT" + ")";
   
    public DatabaseHelper(Context context) {
    
    	super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
	/*public DatabaseHelper(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);

	}*/

	@Override
    public void onCreate(SQLiteDatabase db) 
	{
		db.execSQL(PROJECT_QUERY);
		 
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int arg1, int arg2) {
		// Drop older table if existed
		 db.execSQL("DROP TABLE IF EXISTS " + PROJECT_TABLE);
		  
        // Create tables again
        onCreate(db);
	}
	public void addproject(String url,String email,String password,String desc,String pic1,String pic2,String pic3,String sno,String pno) {
        SQLiteDatabase db = this.getWritableDatabase();
 
        ContentValues values = new ContentValues();
        values.put(URL, url);
       
        values.put(EMAIL, email); // User Name
        values.put(PASSWORD, password); // User Name
        values.put(DESC, desc); // User Name
        values.put(PIC1, pic1); // User Name
        values.put(PIC2, pic2); // User Name
        values.put(PIC3, pic3); // User Name
        values.put(SERIALNO,sno);
        values.put(PARTNO,sno);
        
        // Inserting Row
        db.insert(PROJECT_TABLE, null, values);
        db.close(); // Closing database connection
    }
	public ArrayList<Project> getAllProjects() {
	    ArrayList<Project> friendList = new ArrayList<Project>();
	    // Select All Query
	    String selectQuery = "SELECT  * FROM " + PROJECT_TABLE;
	 
	    SQLiteDatabase db = this.getWritableDatabase();
	    Cursor cursor = db.rawQuery(selectQuery, null);
	 
	    // looping through all rows and adding to list
	    if (cursor.moveToFirst()) {
	        do {
	        	Project friend = new Project();
	            friend.set_id(Integer.parseInt(cursor.getString(0)));
	            friend.set_url(cursor.getString(1));
	            friend.set_email(cursor.getString(2));
	            friend.set_password(cursor.getString(3));
	            friend.set_desc((cursor.getString(4)));
	            friend.set_pic1(cursor.getString(5));
	            friend.set_pic2(cursor.getString(6));
	            friend.set_pic3(cursor.getString(7));
	            friend.set_serialno(cursor.getString(8));
	            friend.set_partno(cursor.getString(9));
		         // Adding contact to list
	            friendList.add(friend);
	        } while (cursor.moveToNext());
	    }
	 Log.d("size",String.valueOf(friendList.size()));
	    // return Friend list
	    return friendList;
	}
	
	public void deleteProject(String des) {
	    SQLiteDatabase db = this.getWritableDatabase();
	    db.delete(PROJECT_TABLE, DESC + " = ?",
	            new String[] { String.valueOf(des) });
	    db.close();
	}
	
	public int getdescID(String desc)
	{ // Getting Friends id
	        SQLiteDatabase db = this.getReadableDatabase();
	 int friendid=45;
	        Cursor cursor = db.query(PROJECT_TABLE, new String[] { ID,URL,
	                EMAIL,PASSWORD,DESC,PIC1,PIC2,PIC3,SERIALNO,PARTNO }, DESC + "=?",
	                new String[] { String.valueOf(desc) }, null, null, null, null);
	        if (cursor != null)
	        {    cursor.moveToFirst();
	 
	             friendid =   Integer.parseInt(cursor.getString(0));
	             // String eventname =  cursor.getString(1);
	      //  return eventid;
	    db.close();
	    }
	        
		return friendid;
	}
	public ArrayList<PicturePathString> getAllProjectsinString() {
	    ArrayList<PicturePathString> friendList = new ArrayList<PicturePathString>();
	    // Select All Query
	    String selectQuery = "SELECT  * FROM " + PROJECT_TABLE;
	 
	    SQLiteDatabase db = this.getWritableDatabase();
	    Cursor cursor = db.rawQuery(selectQuery, null);
	 
	    // looping through all rows and adding to list
	    if (cursor.moveToFirst()) {
	        do {
	        	PicturePathString friend = new PicturePathString();
	            friend.set_id(Integer.parseInt(cursor.getString(0)));
	            friend.set_url(cursor.getString(1));
	            friend.set_email(cursor.getString(2));
	            friend.set_password(cursor.getString(3));
	            friend.set_desc((cursor.getString(4)));
	            friend.set_pic1(cursor.getString(5));
	            friend.set_pic2(cursor.getString(6));
	            friend.set_pic3(cursor.getString(7));
	            friend.set_serialno(cursor.getString(8));
	            friend.set_partno(cursor.getString(9));
	            
		         // Adding contact to list
	            friendList.add(friend);
	        } while (cursor.moveToNext());
	    }
	 
	    // return Friend list
	    return friendList;
	}
	
}
	/*
	//Get Event Id
	public int getEventId(String eventname)
	{ // Getting Event id
	        SQLiteDatabase db = this.getReadableDatabase();
	 int eventid=45;
	        Cursor cursor = db.query(EVENTS_TABLE, new String[] { EVENTS_ID,
	                EVENTS_NAME,EVENTS_CREATOR }, EVENTS_NAME + "=?",
	                new String[] { String.valueOf(eventname) }, null, null, null, null);
	        if (cursor != null)
	        {    cursor.moveToFirst();
	 
	             eventid =   Integer.parseInt(cursor.getString(0));
	             // String eventname =  cursor.getString(1);
	      //  return eventid;
	    db.close();
	    }
	        
		return eventid;
	}

	//Get friend Id of friend bu passing name
	public int getFriendId(String friendname)
	{ // Getting Friends id
	        SQLiteDatabase db = this.getReadableDatabase();
	 int friendid=45;
	        Cursor cursor = db.query(FRIENDS_TABLE, new String[] { FRIENDS_ID,FRIENDS_CID,
	                FRIENDS_NAME,FRIENDS_EMAIL,FRIENDS_CURRENCY,FRIENDS_COEFFICIENT }, FRIENDS_NAME + "=?",
	                new String[] { String.valueOf(friendname) }, null, null, null, null);
	        if (cursor != null)
	        {    cursor.moveToFirst();
	 
	             friendid =   Integer.parseInt(cursor.getString(0));
	             // String eventname =  cursor.getString(1);
	      //  return eventid;
	    db.close();
	    }
	        
		return friendid;
	}
	//Reading all friends
	public List<Friends> getAllFriends() {
	    List<Friends> friendList = new ArrayList<Friends>();
	    // Select All Query
	    String selectQuery = "SELECT  * FROM " + FRIENDS_TABLE;
	 
	    SQLiteDatabase db = this.getWritableDatabase();
	    Cursor cursor = db.rawQuery(selectQuery, null);
	 
	    // looping through all rows and adding to list
	    if (cursor.moveToFirst()) {
	        do {
	            Friends friend = new Friends();
	            friend.set_friends_id(Integer.parseInt(cursor.getString(0)));
	            friend.set_friends_cid(Integer.parseInt(cursor.getString(1)));
	            friend.set_friends_name(cursor.getString(2));
	            friend.set_friends_email(cursor.getString(3));
	            friend.set_friends_currency((cursor.getString(4)));
	            friend.set_friends_coefficient(Float.parseFloat(cursor.getString(5)));
	            // Adding contact to list
	            friendList.add(friend);
	        } while (cursor.moveToNext());
	    }
	 
	    // return Friend list
	    return friendList;
	}
	 public void addProfile(Profile p) {
	        SQLiteDatabase db = this.getWritableDatabase();
	 
	        ContentValues values = new ContentValues();
	        values.put(PROFILE_NAME, p.get_profile_user_name()); // User Name
	 
	        // Inserting Row
	        db.insert(PROFILE_TABLE, null, values);
	        db.close(); // Closing database connection
	    }
	 public void addFriend(Friends f) {
	        SQLiteDatabase db = this.getWritableDatabase();
	 
	        ContentValues values = new ContentValues();
	        values.put(FRIENDS_CID, f.get_friends_name()); // Friend CID
	        values.put(FRIENDS_NAME, f.get_friends_name()); // Friend Name
	        values.put(FRIENDS_EMAIL, f.get_friends_email()); // Friend email
	        values.put(FRIENDS_CURRENCY, f.get_friends_currency()); // Friend Currency
	        values.put(FRIENDS_COEFFICIENT, f.get_friends_coefficient()); // Friend coeffiecient
	       
	        // Inserting Row
	        db.insert(FRIENDS_TABLE, null, values);
	        db.close(); // Closing database connection
	    }
	 public void addEvent(Events e) {
	        SQLiteDatabase db = this.getWritableDatabase();
	    	
	        ContentValues values = new ContentValues();
	      //  values.put(EVENTS_ID, f.get_friends_name()); // EVENT ID
	        values.put(EVENTS_NAME, e.get_events_name()); //  EVENT Name
	        values.put(EVENTS_CREATOR, e.get_events_creator()); //  EVENT creator name
		      
	        // Inserting Rows
	        db.insert(EVENTS_TABLE, null, values);
	        db.close(); // Closing database connection
	    }


		//Reading all Events
		public List<Events> getAllEvents() {
		    List<Events> eventList = new ArrayList<Events>();
		    // Select All Query
		    String selectQuery = "SELECT * FROM " + EVENTS_TABLE;
		 
		    SQLiteDatabase db = this.getWritableDatabase();
		    Cursor cursor = db.rawQuery(selectQuery, null);
		 
		    // looping through all rows and adding to list
		    if (cursor.moveToFirst()) {
		        do {
		            Events event = new Events();
		            event.set_events_id(Integer.parseInt(cursor.getString(0)));
		            event.set_events_name(cursor.getString(1));
		            event.set_events_creator(cursor.getString(2));
		            
		            // Adding events to list
		            eventList.add(event);
		        } while (cursor.moveToNext());
		    }
		 
		    // return Event list
		    return eventList;
		}
		
		
		public void addRelation(Events e,Friends f) {
		        SQLiteDatabase db = this.getWritableDatabase();
		    	
		        ContentValues values = new ContentValues();
		      //  values.put(RELATIONS_ID, f.get_friends_name()); // RELATION ID PK
		        
		        values.put(RELATIONS_EVENTS_ID, e.get_events_id()); //  relation event id	
		        values.put(RELATIONS_EVENTS_NAME, e.get_events_name()); //  relation event name	
		        values.put(RELATIONS_EVENTS_CREATOR, e.get_events_creator()); //  relation event creator name	
		       // values.put(RELATIONS_FRIENDS_ID, f.get_friends_id()); //  relation friend id	
		        values.put(RELATIONS_FRIENDS_CID, f.get_friends_cid()); //  relation friend cid	
			       
		        values.put(RELATIONS_FRIENDS_NAME, f.get_friends_name()); //  relation friend name	
		        values.put(RELATIONS_FRIENDS_EMAIL, f.get_friends_email()); //  relation friend email	
		        values.put(RELATIONS_FRIENDS_CURRENCY, f.get_friends_currency()); //  relation friend email	
			    values.put(RELATIONS_FRIENDS_COEFFICIENT, f.get_friends_coefficient()); //  relation friend phone	
		        // Inserting Row
		        db.insert(RELATIONS_TABLE, null, values);
		        db.close(); // Closing database connection
		    }


		//Reading all Relations
		public List<Relations> getAllRelations() {
		    List<Relations> relationList = new ArrayList<Relations>();
		    // Select All Query
		    String selectQuery = "SELECT * FROM " + RELATIONS_TABLE;
		 
		    SQLiteDatabase db = this.getWritableDatabase();
		    Cursor cursor = db.rawQuery(selectQuery, null);
		 
		    // looping through all rows and adding to list
		    if (cursor.moveToFirst()) {
		        do {
		            Relations relation= new Relations();
		            relation.set_relation_id(Integer.parseInt(cursor.getString(0)));
		            relation.set_relation_events_id(Integer.parseInt(cursor.getString(1)));
		            relation.set_relation_events_name(cursor.getString(2));
		            relation.set_relation_events_creator(cursor.getString(3));
		          //  relation.set_relation_friends_id(Integer.parseInt(cursor.getString(4)));
		            relation.set_relation_friends_cid(Integer.parseInt(cursor.getString(4)));
		            relation.set_relation_friends_name(cursor.getString(5));
		            relation.set_relation_friends_email(cursor.getString(6));
		            relation.set_relation_friends_currency(cursor.getString(7));
		            relation.set_relation_friends_coeffiecients(Float.parseFloat(cursor.getString(8)));
		            
		            // Adding relation to list
		            relationList.add(relation);
		        } while (cursor.moveToNext());
		    }
		 
		    // return relation list
		    return relationList;
		}

		//Reading all Relations
				public List<Relations> getMatchRelations(String eventname) {
				    List<Relations> relationList = new ArrayList<Relations>();
				    int count = 0;
				    // Select All Query
				    String selectQuery = "SELECT * FROM " + RELATIONS_TABLE + " WHERE "+RELATIONS_EVENTS_NAME+" = " + eventname;
				 
				    SQLiteDatabase db = this.getWritableDatabase();
				 //   Cursor cursor = db.rawQuery(selectQuery, null);
				    Cursor cursor = db.query(RELATIONS_TABLE, new String[] { RELATIONS_ID,RELATIONS_EVENTS_ID,RELATIONS_EVENTS_NAME,RELATIONS_EVENTS_CREATOR,
				    		RELATIONS_FRIENDS_CID,RELATIONS_FRIENDS_NAME,RELATIONS_FRIENDS_EMAIL,RELATIONS_FRIENDS_CURRENCY,RELATIONS_FRIENDS_COEFFICIENT }, RELATIONS_EVENTS_NAME + "=?",
			                new String[] { String.valueOf(eventname) }, null, null, null, null);
				 
				    // looping through all rows and adding to list
				    if (cursor.moveToFirst()) {
				        do { count= count+1;
				            Relations relation= new Relations();
				            relation.set_relation_id(Integer.parseInt(cursor.getString(0)));
				            relation.set_relation_events_id(Integer.parseInt(cursor.getString(1)));
				            relation.set_relation_events_name(cursor.getString(2));
				            relation.set_relation_events_creator(cursor.getString(3));
				          //  relation.set_relation_friends_id(Integer.parseInt(cursor.getString(4)));
				            relation.set_relation_friends_cid(Integer.parseInt(cursor.getString(4)));
				            relation.set_relation_friends_name(cursor.getString(5));
				            relation.set_relation_friends_email(cursor.getString(6));
				            relation.set_relation_friends_currency(cursor.getString(7));
				            relation.set_relation_friends_coeffiecients(Float.parseFloat(cursor.getString(8)));
				            
				            // Adding relation to list
				            relationList.add(relation);
				        } while (cursor.moveToNext());
				    }
				    Log.d("count",String.valueOf(count));
				 
				    // return relation list
				    return relationList;
				}


}
*/