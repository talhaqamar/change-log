package com.oliver.project.database;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.oliver.project.MainActivity;
import com.oliver.project.R;



public class CustomBaseAdapter extends BaseAdapter { //implements OnClickListener {
	private static ArrayList<Project> searchArrayList;
	Context mycontext;
	private LayoutInflater mInflater;
	ViewHolder holder;
	static int pos;
	public CustomBaseAdapter(Context context, ArrayList<Project> results) {
		mycontext = context;
		searchArrayList = results;
		mInflater = LayoutInflater.from(context);
	}
	public ArrayList<Project> updateReceiptsList(ArrayList<Project> newlist,String desc) {
	    searchArrayList.clear();
	   
	    DatabaseHelper  d = new DatabaseHelper(mycontext);
	    d.deleteProject(desc);

	    MainActivity.desc = "";
	    searchArrayList.addAll(d.getAllProjects());
	    d.close();
	    Log.d("updatereceipt",String.valueOf(MainActivity.id)+" "+desc);
	    if(searchArrayList.size() == 0){ MainActivity.uploadmain.setEnabled(true);}
	    this.notifyDataSetChanged();
	    return searchArrayList;
	    //
	}
	public void clearList() {
	    searchArrayList.clear();
	    this.notifyDataSetChanged();
	}
	public int getCount() {
		return searchArrayList.size();
	}

	public Object getItem(int position) {
		return searchArrayList.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.customview, null);
			holder = new ViewHolder();
			holder.customtextview = (TextView) convertView.findViewById(R.id.customtextview);
			holder.firstimage = (ImageView) convertView.findViewById(R.id.firstimage);
			holder.secondimage = (ImageView) convertView.findViewById(R.id.secondimage);
			holder.thirdimage = (ImageView) convertView.findViewById(R.id.thirdimage);
			holder.garbage = (ImageView) convertView.findViewById(R.id.garbage);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.customtextview.setText(searchArrayList.get(position).get_desc());
		
		if(searchArrayList.get(position).get_pic1() != null)
		{
			holder.firstimage.setImageBitmap(searchArrayList.get(position).get_pic1());
		}
		else
		{
			Bitmap b1 = BitmapFactory.decodeResource(mycontext.getResources(),R.drawable.back);
			holder.firstimage.setImageBitmap(b1);
			
		}
		if(searchArrayList.get(position).get_pic2()!=null){
		holder.secondimage.setImageBitmap(searchArrayList.get(position).get_pic2());}
		else {
			Bitmap b2 = BitmapFactory.decodeResource(mycontext.getResources(),R.drawable.back);
			holder.secondimage.setImageBitmap(b2);
			}
		if(searchArrayList.get(position).get_pic3()!=null)
		{
		holder.thirdimage.setImageBitmap(searchArrayList.get(position).get_pic3());
		}
		else
		{
			Bitmap b3 = BitmapFactory.decodeResource(mycontext.getResources(),R.drawable.back);
			holder.thirdimage.setImageBitmap(b3);
				
		}
		holder.garbage.setImageResource(R.drawable.garbage);
		//holder.garbage.setOnClickListener(this);
		holder.garbage.setContentDescription(holder.customtextview.getText().toString());
		holder.garbage.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
			
				String des =  v.getContentDescription().toString();
				MainActivity.db.deleteProject(des);
				
				searchArrayList.clear();
			    searchArrayList.addAll(MainActivity.db.getAllProjects());
			   notifyDataSetChanged();
			
				Toast.makeText(mycontext, ""+ v.getContentDescription().toString(), 5000).show();
		
			}
		});

		return convertView;
	}

	static class ViewHolder {
		TextView customtextview;
		ImageView firstimage;
		ImageView secondimage;
		ImageView thirdimage;
		ImageView garbage;
		
	}

	/*@Override
	public void onClick(View v) {
		int position = (Integer)v.getTag();
		Toast.makeText(mycontext, "" + String.valueOf(position), 5000).show();
	}*/
}

