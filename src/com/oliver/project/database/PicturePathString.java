package com.oliver.project.database;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class PicturePathString {
	 int id;
	 String url;
	 String email;
	 String password;
	 String desc;
	 String pic1;
	 String pic2;
	 String pic3;
	 String serialno;
	 String partno;
	 
	 PicturePathString(int id,String url,String email,String pass,String desc,String p1,String p2,String p3,String sno,String pno)
	 {
		 this.id = id;
		 this.url = url;
		 this.email = email;
		 this.password = pass;
		 this.desc = desc;
		 this.pic1 = p1;
		 this.pic2 = p2;
		 this.pic3 = p3;
		 this.serialno = sno;
		 this.partno = pno;
	 }
	 PicturePathString(String url,String email,String pass,String desc,String p1,String p2,String p3)
	 {
		 this.url = url;
		 this.email = email;
		 this.password = pass;
		 this.desc = desc;
		 this.pic1 = p1;
		 this.pic2 = p2;
		 this.pic3 = p3;
		 
	 }
	  
		public PicturePathString(){}
		public void set_id(int id){this.id = id;}
		public int get_id(){return this.id;}
		
		public void set_url(String url){this.url = url;}
		public String get_url(){return this.url;}

		public void set_email(String email){this.email = email;}
		public String get_email(){return this.email;}
		
		public void set_password(String fe){this.password = fe;}
		public String get_password(){return this.password;}
		
		public void set_desc(String desc){this.desc = desc;}
		public String get_desc(){return this.desc;}
		
		public void set_pic1(String pic1){this.pic1 = pic1;}
		public String get_pic1(){
		//	Bitmap b = BitmapFactory.decodeFile(this.pic1);
			
			return this.pic1;}

		public void set_pic2(String pic2){this.pic2 = pic2;}
		public String get_pic2(){
			//Bitmap b = BitmapFactory.decodeFile(this.pic2);
			return this.pic2;
			}

		public void set_pic3(String pic3){this.pic3 = pic3;}
		public String get_pic3(){
		//	Bitmap b = BitmapFactory.decodeFile(this.pic3);
			return this.pic3;
			}
		public void set_serialno(String sno){this.serialno = sno;}
		public String get_serialno(){return this.serialno;}
		public void set_partno(String pno){this.partno = pno;}
		public String get_partno(){return this.partno;}


}
