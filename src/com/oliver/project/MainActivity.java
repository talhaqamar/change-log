package com.oliver.project;

import java.io.File;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.oliver.project.database.CustomBaseAdapter;
import com.oliver.project.database.DatabaseHelper;
import com.oliver.project.database.PicturePathString;
import com.oliver.project.database.Project;
import com.oliver.project.https.HttpHandler;
import com.oliver.project.https.HttpListner;

public class MainActivity extends Activity implements OnClickListener,HttpListner {
	static Button credentialsmain,exitmain,newchange;
	public static Button uploadmain;
	static SharedPreferences prefs =null;
	TextView createdtextview;
	public static DatabaseHelper db = null;
	ListView l=null;
	public int ID=0;
	static String mydesc="";
	static int myID=0;
	ArrayList<PicturePathString> uploadlist = null;
	ProgressDialog  progressDialog= null;
	private static final int SELECT_FILE1 = 1;
	private static final int SELECT_FILE2 = 2;
	String selectedPath1 = "NONE";
	String selectedPath2 = "NONE";
	HttpEntity resEntity;
	public static int id=0;
	public static String url=null; 
     public static String email =null;
     public static String password =null;
     public static String desc =null;
    public static String path1 =null;
     public static String path2 =null;
    public static String path3 =null;
    public static String serialno= null;
    public static String partno = null;
    public static CustomBaseAdapter cba = null;
    public static ArrayList<Project> searchResults= null;
    MultipartEntity reqEntity= null;
    FileBody bin1 = null;
    FileBody bin2 = null;
    FileBody bin3 = null;
    File file1 = null;
    File file2 = null;
    File file3 = null;
    ListView lv1;
   int GLOBAL=0;
   public static ProgressDialog dialog = null;
    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.fragment_main);

		initialize();
		setOnclicklistner();
		prefs = this.getSharedPreferences(
			      "com.oliver.project", Context.MODE_WORLD_WRITEABLE);
		
		
		 searchResults = db.getAllProjects();
	        		db.close();
	        	cba = 	new CustomBaseAdapter(this, searchResults);
	       //	cba.notifyDataSetChanged();
	         lv1 = (ListView) findViewById(R.id.listview);
	        lv1.setAdapter(cba);
	       
	        lv1.setOnItemClickListener(new OnItemClickListener() {
	        	@Override
	        	public void onItemClick(AdapterView<?> a, View v, int position, long id) { 
	
	        	}  
	        });
		
		
	}
	

	private void setOnclicklistner() {
		credentialsmain.setOnClickListener(this);
		uploadmain.setOnClickListener(this);
		exitmain.setOnClickListener(this);
		newchange.setOnClickListener(this);
	}

	private void initialize() {
		createdtextview = (TextView)findViewById(R.id.createdtextview);
		credentialsmain = (Button)findViewById(R.id.credentialsmain);
		uploadmain= (Button)findViewById(R.id.uploadmain);
		exitmain= (Button)findViewById(R.id.exitmain);
		newchange= (Button)findViewById(R.id.changemain);
		db = new DatabaseHelper(this);
		l = (ListView)findViewById(R.id.listview);
		dialog = new ProgressDialog(MainActivity.this);
		   dialog.setMessage("Uploading...");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
@Override
protected void onPause() {
Log.d("onPause","pause");
super.onPause();
}
@Override
protected void onResume() {
	//recreate();
super.onResume();
}
	/**
	 * A placeholder fragment containing a simple view.
	 */


	
	private void doFileUpload( String ID,String url,String email,String password,String desc,String path1,String path2,String path3,String sno,String pno){
		  MainActivity.mydesc = desc;
		  MainActivity.myID = Integer.parseInt(ID);
         
		  MainActivity.id = Integer.parseInt(ID);
          MainActivity.url = url;
          MainActivity.email = email;
          MainActivity.password = password;
          MainActivity.desc = desc;
          MainActivity.path1 = path1;
          MainActivity.path2 =path2;
          MainActivity.path3 = path3;
         MainActivity.serialno = sno;
         MainActivity.partno = pno;
        String urlString = url;//"http://10.0.2.2/upload_test/upload_media.php";
        HttpHandler handler = new HttpHandler(url//"https://62.75.210.58/change-server/develop/oneShot.php"
				//	edtTextUrl.getText().toString().trim()
					,null, null, 123,this,MainActivity.this);
			handler.addHttpLisner(MainActivity.this);
			handler.sendRequest(url,email,password,desc,path1,path2,path3,sno,pno);
	
		}
	
	public static boolean deleteDirectory(File path) {
	    if( path.exists() ) {
	      File[] files = path.listFiles();
	      if (files == null) {
	          return true;
	      }
	      for(int i=0; i<files.length; i++) {
	         if(files[i].isDirectory()) {
	           deleteDirectory(files[i]);
	         }
	         else {
	           files[i].delete();
	      
	         }
	      }
	    } 
	    return( path.delete() );
	  }
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.credentialsmain:
			Intent in = new Intent(MainActivity.this,Credentials.class);
			startActivity(in);
			this.finish();
			break;
		case R.id.uploadmain:
			if((MainActivity.prefs.getString("url", "") != null) && (MainActivity.prefs.getString("email", "") != null) && (MainActivity.prefs.getString("password", "") != null)){
				uploadlist = new ArrayList<PicturePathString>();
				uploadlist = db.getAllProjectsinString();
				Log.d("uploadsize",String.valueOf(uploadlist.size()));
				db.close();
				if(!uploadlist.isEmpty()){
				//	dialog.show();
				uploadmain.setEnabled(false);
					for(int i=uploadlist.size()-1;i>=0;i--)
					{ 
						
						PicturePathString p = uploadlist.get(i);
					     String[] myTaskParams = {String.valueOf(p.get_id()),p.get_url(),p.get_email(),p.get_password(),p.get_desc()
			            		   ,p.get_pic1(),p.get_pic2(),p.get_pic3(),p.get_serialno(),p.get_partno()};
			               Log.d("test",myTaskParams[GLOBAL].toString()+"\n");
			               new LongOperation().execute(myTaskParams);
					}
			
				}
				else
				{
					Toast.makeText(MainActivity.this,"Please take some pictures before uploading", 5000).show();
				}
				
			}
			else 
			{
				Toast.makeText(MainActivity.this,"Please set your url,email and password first then upload", 5000).show();
				
				
			}
			break;
		case R.id.exitmain:
			this.finish();
			break;
		case R.id.changemain:
			Intent in1 = new Intent(MainActivity.this,Change.class);
			startActivity(in1);
			this.finish();
			break;
		default:
			break;
		}
	}
	private class LongOperation extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
        	String id,url,email,password,desc,path1,path2,path3,serialno,partno; 
        	id = params[0];
        	url = params[1];
        	email = params[2];
        	password = params[3];
        	desc = params[4];
        	path1 = params[5];
        	path2 = params[6];
        	path3 = params[7];
        	serialno = params[8];
        	partno = params[9];
        	
        	doFileUpload(id,url,email,password,desc,path1,path2,path3,serialno,partno);
            try {
				Thread.sleep(3000);
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
        	
        	
        
        }

        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}
    
}
	@Override
	public void notifyHTTPRespons(HttpHandler http) {
		// TODO Auto-generated method stub
		
	}
	

}
