package com.oliver.project.https;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.params.ConnManagerPNames;
import org.apache.http.conn.params.ConnPerRouteBean;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.util.EntityUtils;

import android.app.Activity;
import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.oliver.project.MainActivity;

public class HttpHandler {

	/**
	 * refrence of HttpListener interface
	 */
	HttpListner httpListener;
	MultipartEntity reqEntity;
	File file1,file2,file3;
	FileBody bin1,bin2,bin3;
	
	/**
	 * url for example http://wwww.google.com
	 */
	String urlstring = "";
	/**
	 * past request parameter items
	 */
	List<NameValuePair> postRequestParameters = null;
	/**
	 * Http unique reqest id
	 */
	int reqId = 0;
	/**
	 * hold the http response
	 */
	String resMessage = "No Response Please check Url or it may be https certificate issue.";
	/**
	 * response code
	 */
	int resCode = -1;
	Hashtable<String, String> header = null;
	Context mycontext;
	/**
	 * @param urlstring
	 *            requested url
	 * @param requestParameters
	 *            list post parameters if get request then null
	 * @param header
	 *            list of header
	 * @param reqId
	 *            url request id
	 */
	Activity act;
	public HttpHandler(String urlstring,
			final List<NameValuePair> requestParameters,
			final Hashtable<String, String> header, int reqId,Context context,Activity a) {
		this.urlstring = urlstring;
		this.postRequestParameters = requestParameters;
		this.reqId = reqId;
		this.header = header;
		this.mycontext = context;
		this.act = a;
	}

	/**
	 * @return reqest id for request
	 */
	public int getReqId() {
		return reqId;
	}

	/**
	 * Return requested url
	 * 
	 * @return
	 */
	public String getURL() {
		return urlstring;
	}

	/**
	 * @return the response
	 */
	public String getResponse() {

		return resMessage;
	}

	/**
	 * Return Response Code
	 * 
	 * @return
	 */
	public int getResCode() {
		return resCode;
	}

	/**
	 * @param httpListener
	 *            add the listener for notify the response
	 */
	public void addHttpLisner(HttpListner httpListener) {
		this.httpListener = httpListener;
	}

	/**
	 * send the http or https request
	 */

	public void sendRequest(String url,String email,String password,String desc,String path1,String path2,String path3,String sno,String pno) {
		// TODO Auto-generated method stub
		try {
			
			reqEntity = new MultipartEntity();
            
            if(path1.length() >1){
           	 file1 = new File(path1);
           	 bin1 = new FileBody(file1, "image/jpeg");
           	 reqEntity.addPart("1", bin1);
           	 Log.d("file1","file1");}
            
            if(path2.length() >1){
                file2 = new File(path2);
                bin2 = new FileBody(file2, "image/jpeg");
                reqEntity.addPart("2", bin2);
                Log.d("file2","file2");}
            
            if(path3.length() >1)
            {
           	 file3 = new File(path3);
           	 bin3 = new FileBody(file3, "image/jpeg");
                reqEntity.addPart("3", bin3);
                Log.d("file3","file3");}
            
            reqEntity.addPart("email", new StringBody(email));
            reqEntity.addPart("password", new StringBody(password));
            reqEntity.addPart("name", new StringBody(desc));//MainActivity.prefs.getString("desc", "")));
            reqEntity.addPart("serialNumber", new StringBody(sno));
            reqEntity.addPart("partNumber", new StringBody(pno));//MainActivity.prefs.getString("desc", "")));
           reqEntity.addPart("submit", new StringBody("submit checked"));

            postRequestParameters = new ArrayList<NameValuePair>();
            postRequestParameters.add(new BasicNameValuePair("submit", "submit"));

			SchemeRegistry schemeRegistry = new SchemeRegistry();
			schemeRegistry.register(new Scheme("http", PlainSocketFactory
					.getSocketFactory(), 80));
			schemeRegistry.register(new Scheme("https",
					new EasySSLSocketFactory(), 443));

			HttpParams params = new BasicHttpParams();
			params.setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 50000);
			params.setParameter(ConnManagerPNames.MAX_TOTAL_CONNECTIONS, 30);
			params.setParameter(ConnManagerPNames.MAX_CONNECTIONS_PER_ROUTE,
					new ConnPerRouteBean(30));
			params.setParameter(HttpProtocolParams.USE_EXPECT_CONTINUE, false);
			HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
			ClientConnectionManager cm = new SingleClientConnManager(params,
					schemeRegistry);
			HttpClient httpclient = new DefaultHttpClient(cm, params);
			// DefaultHttpClient httpclient = null;
			if (postRequestParameters != null) {// //////// for post request
				// POST the envelope
				HttpPost httppost = new HttpPost(urlstring);
				if (header != null) {
					Enumeration enums = header.keys();
					while (enums.hasMoreElements()) {
						String key = (String) enums.nextElement();

						String value = header.get(key);
						httppost.addHeader(key, value);
					}
				}
				httppost.setEntity(reqEntity);
				// Response handler
				ResponseHandler<String> reshandler = new ResponseHandler<String>() {
					// invoked when client receives response
					public String handleResponse(HttpResponse response)
							throws ClientProtocolException, IOException {
						// get response entity
						HttpEntity entity = response.getEntity();
						// get response code
						resCode = response.getStatusLine().getStatusCode();
						// read the response as byte array
						StringBuffer out = new StringBuffer();
						byte[] b = EntityUtils.toByteArray(entity);
						// write the response byte array to a string buffer
						out.append(new String(b, 0, b.length));
						return out.toString();
					}
				};

				resMessage = httpclient.execute(httppost, reshandler);
				 Log.d("Response=====" ,resMessage);
				 /////////////
				 if(resMessage.contains("0"))
                 {	
                 	if(!MainActivity.searchResults.isEmpty())
                 	{
                 		act.runOnUiThread(new Runnable() {
                            public void run()
                            {
                        	MainActivity.cba.updateReceiptsList(MainActivity.searchResults,MainActivity.desc);
                            Toast.makeText(act, "Uploaded and deleted", Toast.LENGTH_SHORT).show();
                            }
                        });
                 	Thread.sleep(2000);
                 	String root = Environment.getExternalStorageDirectory().toString();
              	    File myDir = new File(root + "/"+MainActivity.desc);
              	   if (myDir.isDirectory()) {
              	        String[] children = myDir.list();
              	        for (int i = 0; i < children.length; i++) {
              	            new File(myDir, children[i]).delete();
              	        }
              	        Toast.makeText(mycontext,"Deleted From Sdcard too!!", Toast.LENGTH_SHORT).show();
              	    }
                 	}
                 	
                 	
                 }
                 else if(resMessage.equals("1")) 
                 {
                 	Toast.makeText(mycontext,"Username or password is invalid", 5000).show();
                 	
                 	
                 }
                 else if(resMessage.equals("2"))
                 {
                 	Toast.makeText(mycontext,"No data", 5000).show();
                 	
                 }
                 else {
                 	Toast.makeText(mycontext,"The url is wrong", 5000).show()
                 	;
                 	
                 }
				 
				 
				 ////////////

			} else {// ///////// for get Request

				ResponseHandler<String> responsehandler = new ResponseHandler<String>() {

					@Override
					public String handleResponse(HttpResponse response)
							throws ClientProtocolException, IOException {
						// TODO Auto-generated method stub
						HttpEntity entity = response.getEntity();
						// get response code
						resCode = response.getStatusLine().getStatusCode();
						// read the response as byte array
						StringBuffer out = new StringBuffer();
						byte[] b = EntityUtils.toByteArray(entity);
						// write the response byte array to a string buffer
						out.append(new String(b, 0, b.length));
						return out.toString();
					}
				};
				HttpGet httpget = new HttpGet(urlstring);
				resMessage = httpclient.execute(httpget, responsehandler);
				 Log.d("Response===== else" ,resMessage);

			}
			// close the connection
			httpclient.getConnectionManager().shutdown();
		} catch (Exception e) {
			Log.i("connection Exeception", e.getMessage());
		} finally {

			httpListener.notifyHTTPRespons(this);
		}

	}

}
