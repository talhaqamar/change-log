package com.oliver.project;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class Credentials extends Activity implements OnClickListener {
	EditText urledittext,emailedittext,passwordedittext;

	Button savecredentials;
	
@Override
protected void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onCreate(savedInstanceState);
	setContentView(R.layout.credentials);
	
	
	initialize();
	setOnclicklistner();
	if(!MainActivity.prefs.getString("url", "").isEmpty() 
			&& !MainActivity.prefs.getString("email", "").isEmpty() 
			&& !MainActivity.prefs.getString("password", "").isEmpty()
			
	   )
	{
		urledittext.setText(MainActivity.prefs.getString("url", "").toString());
		emailedittext.setText(MainActivity.prefs.getString("email", "").toString());
		passwordedittext.setText(MainActivity.prefs.getString("password", "").toString());
	
	}
}

private void setOnclicklistner() 
{
	savecredentials.setOnClickListener(this);
	
}

private void initialize() {
		urledittext = (EditText)findViewById(R.id.urledittext);
		emailedittext  = (EditText)findViewById(R.id.emailedittext);
		passwordedittext  = (EditText)findViewById(R.id.passwordedittext);
		savecredentials = (Button)findViewById(R.id.savecredentialsbutton);
}
public final static boolean isValidEmail(CharSequence target) {
	  if (TextUtils.isEmpty(target)) {
	    return false;
	  } else {
	    return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
	  }
	}

@Override
public void onClick(View v) {
	switch (v.getId()) {
	case R.id.savecredentialsbutton:
		if(urledittext.getText().toString().length() == 0){urledittext.setError("Please enter url");}
		else if(urledittext.getText().toString().length() != 0){ boolean urlcheck = URLUtil.isValidUrl(urledittext.getText().toString()); if(!urlcheck){urledittext.setError("Your url is not valid");} }
		
		 if(emailedittext.getText().toString().length() == 0){emailedittext.setError("Please enter Email Address");}
		else if(emailedittext.getText().toString().length() != 0) { boolean emailcheck = isValidEmail(emailedittext.getText().toString()); if(!emailcheck){emailedittext.setError("Your Email is not valid");} }
		
		 if(passwordedittext.getText().toString().length() == 0){passwordedittext.setError("Please enter Password");}
		
		if(urledittext.getText().toString().length() > 0 && emailedittext.getText().toString().length() > 0 &&
				passwordedittext.getText().toString().length()> 0 ){
			MainActivity.prefs.edit().putString("url", urledittext.getText().toString()).commit();
			MainActivity.prefs.edit().putString("email", emailedittext.getText().toString()).commit();
			MainActivity.prefs.edit().putString("password", passwordedittext.getText().toString()).commit();
		
			
		Toast.makeText(Credentials.this, "Data saved", 5000).show();
		Intent in = new Intent(Credentials.this,MainActivity.class);
		startActivity(in);
		this.finish();
		}
		break;

	
	default:
		break;
	}
}
}
