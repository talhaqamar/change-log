package com.oliver.project;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Random;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.oliver.project.database.DatabaseHelper;

public class Change extends Activity implements OnClickListener {
static ImageView picture1,picture2,picture3;
Button savechange,cancelchange,uploadchange;
   public static int check=0;
DatabaseHelper db=null;
static String path1="";
static String path2="";
static String path3="";
EditText changedescedit;
private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
	
private static final int SELECT_FILE1 = 1;
private static final int SELECT_FILE2 = 2;
String selectedPath1 = "NONE";
String selectedPath2 = "NONE";
HttpEntity resEntity;
ProgressDialog  progressDialog= null;
EditText serialnoedit,partnoedit;
String serialNO="";
String partNO="";
ImageView serialnoimage,partnoimage;
@Override
protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.change);
	
		initialize();
		setOnclicklistner();
		/*if( ( MainActivity.prefs.getString("picture3","")!=null) && (MainActivity.prefs.contains("picture3")) )
    	{
    		Bitmap b = BitmapFactory.decodeFile(MainActivity.prefs.getString("picture3", ""));
			picture3.setImageBitmap(b);
    	}
		if( (MainActivity.prefs.getString("picture2","")!=null) && (MainActivity.prefs.contains("picture2")) )
    	{
    		Bitmap b = BitmapFactory.decodeFile(MainActivity.prefs.getString("picture2", ""));
			picture2.setImageBitmap(b);
    	}
		if( ( MainActivity.prefs.getString("picture1","")!=null) && (MainActivity.prefs.contains("picture1")) )
    	{
    		Bitmap b = BitmapFactory.decodeFile(MainActivity.prefs.getString("picture1", ""));
			picture1.setImageBitmap(b);
    	}*/

}

	private void setOnclicklistner() {
	picture1.setOnClickListener(this);
	picture2.setOnClickListener(this);
	picture3.setOnClickListener(this);
	cancelchange.setOnClickListener(this);
	savechange.setOnClickListener(this);
	//uploadchange.setOnClickListener(this);
	serialnoimage.setOnClickListener(this);
	partnoimage.setOnClickListener(this);
	}
@Override
	public void onBackPressed() {
	Intent in = new Intent(Change.this,MainActivity.class);
	startActivity(in);
	this.finish();
	
	super.onBackPressed();
	}
	private void initialize() {
	picture1 = (ImageView)findViewById(R.id.picture1imageview);
	picture2 = (ImageView)findViewById(R.id.picture2imageview);
	picture3 = (ImageView)findViewById(R.id.picture3imageview);
	savechange = (Button)findViewById(R.id.savechange);
	cancelchange = (Button)findViewById(R.id.cancelchange);
	//uploadchange = (Button)findViewById(R.id.uploadchange);
	
	db = new DatabaseHelper(this);
	changedescedit = (EditText)findViewById(R.id.changeedittext);
	serialnoedit  = (EditText)findViewById(R.id.serialnoedit);
	partnoedit  = (EditText)findViewById(R.id.partnoedit);
	serialnoimage = (ImageView)findViewById(R.id.serialnoimage);
	partnoimage = (ImageView)findViewById(R.id.partnoimage);
	
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.picture1imageview:
			/*Intent intent1 = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent1.putExtra(MediaStore.EXTRA_OUTPUT, getImageUri());
            startActivityForResult(intent1, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);	
	*/	
			if(changedescedit.getText().length() > 0)
			{
				MainActivity.prefs.edit().putString("desc", changedescedit.getText().toString()).commit();
			check=1;
			MainActivity.prefs.edit().remove("picture1").commit();
			
			 Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
			// intent.putExtra(MediaStore.EXTRA_OUTPUT, getImageUri());
		      startActivityForResult(intent, check); }
			else 
			{
				Toast.makeText(Change.this,"Please enter description first then take picture", 5000).show();
			}
			break;
		case R.id.picture2imageview:
			if(changedescedit.getText().length() > 0)
			{
				MainActivity.prefs.edit().putString("desc", changedescedit.getText().toString()).commit();
			check=2;
			MainActivity.prefs.edit().remove("picture2").commit();
			
			Intent intent2 = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
			//intent2.putExtra(MediaStore.EXTRA_OUTPUT, getImageUri());
		      startActivityForResult(intent2, check);
			/*Intent intent2 = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent2.putExtra(MediaStore.EXTRA_OUTPUT, getImageUri());
            startActivityForResult(intent2,CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);	
	*/			}
		      else 
				{
					Toast.makeText(Change.this,"Please enter description first then take picture", 5000).show();
				}
		      
		      break;
		case R.id.picture3imageview:
			if(changedescedit.getText().length() > 0)
			{
				MainActivity.prefs.edit().putString("desc", changedescedit.getText().toString()).commit();
			check=3;
			Change.check = 3;
			Log.d("cehck before onactivityre",String.valueOf(check));
			
			MainActivity.prefs.edit().remove("picture3").commit();
			
			Intent intent3 = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
			//intent3.putExtra(MediaStore.EXTRA_OUTPUT, getImageUri());
		      startActivityForResult(intent3, check);
/*			Intent intent3 = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent3.putExtra(MediaStore.EXTRA_OUTPUT, getImageUri());
            startActivityForResult(intent3, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);*/	
			}
		      else 
				{
					Toast.makeText(Change.this,"Please enter description first then take picture", 5000).show();
				}
		     
		      break;
		case R.id.savechange:
			if(!MainActivity.prefs.getString("url", "").isEmpty() && !MainActivity.prefs.getString("email", "").isEmpty() && !MainActivity.prefs.getString("password", "").isEmpty())
			{
			
			if(Change.path1.isEmpty() && Change.path2.isEmpty() && Change.path3.isEmpty())
			{
				Toast.makeText(Change.this, "Please select atleast one pic", 5000).show();
			}
		else if(!Change.path1.isEmpty() && !Change.path2.isEmpty() && !Change.path3.isEmpty() )
			{
				
				if((MainActivity.prefs.getString("url","")==null) && (MainActivity.prefs.getString("email","")==null) && (MainActivity.prefs.getString("password","")==null) && (MainActivity.prefs.getString("desc","")==null))
				{
					Toast.makeText(Change.this, "Please set Url,Email,Password,Description first then press save pictures", 10000).show();
					
				}
				else 
				{
					MainActivity.prefs.edit().putString("serialno", serialnoedit.getText().toString()).commit();
					MainActivity.prefs.edit().putString("partno", partnoedit.getText().toString()).commit();
					
					String url = MainActivity.prefs.getString("url","");
					String email= MainActivity.prefs.getString("email","");
					String password=MainActivity.prefs.getString("password","");
					String desc=MainActivity.prefs.getString("desc","");
				
					String serialno = serialnoedit.getText().toString();
					String partno = partnoedit.getText().toString();
				
					db.addproject(url, email, password, desc, Change.path1, Change.path2, Change.path3,serialno,partno);
					Toast.makeText(Change.this,"Images saved", 100000).show();
					Change.path1=null;
					Change.path2=null;
					Change.path3=null;
					changedescedit.setText("");
					MainActivity.prefs.edit().remove("desc").commit();
					MainActivity.prefs.edit().remove("picture1").commit();
					MainActivity.prefs.edit().remove("picture2").commit();
					MainActivity.prefs.edit().remove("picture3").commit();
					serialno="";
					partno = "";
					/*	Drawable myDrawable = getResources().getDrawable(R.drawable.photo);
					
					picture1.setImageDrawable(myDrawable);
					picture2.setImageDrawable(myDrawable);
					picture3.setImageDrawable(myDrawable);
				*/	
					Intent in = new Intent(Change.this,MainActivity.class);
					startActivity(in);
					this.finish();
				}
			}
			else 
			{
				MainActivity.prefs.edit().putString("serialno", serialnoedit.getText().toString()).commit();
				MainActivity.prefs.edit().putString("partno", partnoedit.getText().toString()).commit();
				String url = MainActivity.prefs.getString("url","");
				String email= MainActivity.prefs.getString("email","");
				String password=MainActivity.prefs.getString("password","");
				String desc=MainActivity.prefs.getString("desc","");
				String serialno = serialnoedit.getText().toString();
				String partno = partnoedit.getText().toString();
				
				/*	if(Change.path1.isEmpty()){ Change.path1="";}
				if(Change.path2.isEmpty()){Change.path2="";}
				if(Change.path3.isEmpty()){Change.path3="";}
			*/	db.addproject(url, email, password, desc, Change.path1, Change.path2, Change.path3,serialno,partno);
			
				
				Toast.makeText(Change.this,"Images saved", 100000).show();
				
				Change.path1= "";
				Change.path2= "";
				Change.path3= "";
				changedescedit.setText("");
				MainActivity.prefs.edit().remove("desc").commit();
				MainActivity.prefs.edit().remove("picture1").commit();
				MainActivity.prefs.edit().remove("picture2").commit();
				MainActivity.prefs.edit().remove("picture3").commit();
				serialno="";
				partno = "";
				/*	Drawable myDrawable = getResources().getDrawable(R.drawable.photo);
				
				picture1.setImageDrawable(myDrawable);
				picture2.setImageDrawable(myDrawable);
				picture3.setImageDrawable(myDrawable);
			*/	
				Intent in = new Intent(Change.this,MainActivity.class);
				startActivity(in);
				this.finish();
				//Toast.makeText(Change.this,"Please select  "+ String.valueOf(3 - Change.count) +" images then click save", 100000).show();
				
			}
			}
			else 
			{
				Toast.makeText(Change.this,"Please enter the credentials first.", 100000).show();
				
			}
		break;
		/*case R.id.uploadchange:
			  progressDialog = ProgressDialog.show(Change.this, "", "Uploading files to server.....", false);
               Thread thread=new Thread(new Runnable(){
                      public void run(){
                          doFileUpload();
                          runOnUiThread(new Runnable(){
                              public void run() {
                                  if(progressDialog.isShowing())
                                      progressDialog.dismiss();
                              }
                          });
                      }
              });
              thread.start();
			break;*/
		case R.id.cancelchange :
			this.finish();
			break;
		case R.id.serialnoimage:
			try {
				
				Intent intent = new Intent(
						"com.google.zxing.client.android.SCAN");
				intent.putExtra("SCAN_MODE", "QR_CODE_MODE,PRODUCT_MODE");
				startActivityForResult(intent, 11);
			
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Toast.makeText(getApplicationContext(), "ERROR:" + e, 11).show();

			}

			break;
		case R.id.partnoimage:
			try {
				
				Intent intent = new Intent(
						"com.google.zxing.client.android.SCAN");
				intent.putExtra("SCAN_MODE", "QR_CODE_MODE,PRODUCT_MODE");
				startActivityForResult(intent, 22);
			
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Toast.makeText(getApplicationContext(), "ERROR:" + e, 22).show();

			}

			break;
		default:
			break;
		}
	}
	/*private void doFileUpload(){
		 
        File file1 = new File(Change.path1);
        File file2 = new File(Change.path2);
        File file3 = new File(Change.path3);
        String urlString = "https://62.75.210.58/change-server/develop/oneShot.php";//"http://10.0.2.2/upload_test/upload_media.php";
        try
        {
             HttpClient client = new DefaultHttpClient();
             HttpPost post = new HttpPost(urlString);
             FileBody bin1 = new FileBody(file1);
             FileBody bin2 = new FileBody(file2);
             FileBody bin3 = new FileBody(file3);
             MultipartEntity reqEntity = new MultipartEntity();
             reqEntity.addPart("uploadedfile1", bin1);
             reqEntity.addPart("uploadedfile2", bin2);
             reqEntity.addPart("uploadedfile3", bin3);
             reqEntity.addPart("email", new StringBody("talha@a.b"));
             reqEntity.addPart("password", new StringBody("123"));
             reqEntity.addPart("name", new StringBody(MainActivity.prefs.getString("desc", "")));
             reqEntity.addPart("submit", new StringBody("submit checked"));
             post.setEntity(reqEntity);
             HttpResponse response = client.execute(post);
             resEntity = response.getEntity();
            
             final String response_str = EntityUtils.toString(resEntity);
             if (resEntity != null) {
                 Log.i("RESPONSE",response_str);
                 runOnUiThread(new Runnable(){
                        public void run() {
                             try {
                                //res.setTextColor(Color.GREEN);
                               // res.setText("n Response from server : n " + response_str);
                                Toast.makeText(getApplicationContext(),"Upload Complete. Check the server uploads directory.", Toast.LENGTH_LONG).show();
                                Toast.makeText(getApplicationContext(), response_str.toString(), Toast.LENGTH_LONG).show();
                                
                             } catch (Exception e) {
                                e.printStackTrace();
                            }
                           }
                    });
             }
        }
        catch (Exception ex){
             Log.e("Debug", "error: " + ex.getMessage(), ex);
        }
      }
	
	
	*/
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);

	    if ( resultCode == RESULT_OK ) {
	        //Uri imagePath = getImageUri();
	    	Log.d("check",String.valueOf(Change.check));
	    	Log.d("check",String.valueOf(check));
	    	
	    	
	    	if(check==3 && Change.check==3)
	    	{
	    		
	    	if( ( MainActivity.prefs.getString("picture3","")!=null) && (MainActivity.prefs.contains("picture3")) )
	    	{
	    		Bitmap b = BitmapFactory.decodeFile(MainActivity.prefs.getString("picture3", ""));
    			picture3.setImageBitmap(b);
    			Change.path3 = MainActivity.prefs.getString("picture3", "");
	    	}
	    	else {
		      
	    		Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
		        
	        	picture3.setImageBitmap(thumbnail);
	        	String pic3 = SaveImage(thumbnail);
	        	Change.path3 = pic3;
	        	MainActivity.prefs.edit().putString("picture3", "test").commit();
		        MainActivity.prefs.edit().putString("picture3", pic3.toString()).commit();
	        	
	           Change.check =0;
	           }
	    	}
	    	 
	    	
	    	else if(check==2 && Change.check==2){
	    		
	    	if( ( MainActivity.prefs.getString("picture2","")!=null) && (MainActivity.prefs.contains("picture2")) )
	    	{
	    		Bitmap b = BitmapFactory.decodeFile(MainActivity.prefs.getString("picture2", ""));
    			picture2.setImageBitmap(b);
    			Change.path2 = MainActivity.prefs.getString("picture2", "");
	    	}
	    	else {
		      
	    		Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
		        
	        	picture2.setImageBitmap(thumbnail);
	        	String pic2 = SaveImage(thumbnail);
	        	Change.path2 = pic2;
	        	MainActivity.prefs.edit().putString("picture2", "test").commit();
		        MainActivity.prefs.edit().putString("picture2", pic2.toString()).commit();
	        	
	           Change.check =0;
	           }
	    	}
	    	else if(Change.check ==1){
	    	if( ( MainActivity.prefs.getString("picture1","")!=null) && (MainActivity.prefs.contains("picture1")) )
	    	{
	    		Bitmap b = BitmapFactory.decodeFile(MainActivity.prefs.getString("picture1", ""));
    			picture1.setImageBitmap(b);
    			Change.path1 = MainActivity.prefs.getString("picture1", "");
	    	}
	    	else {
		      
	    		Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
		        
	        	picture1.setImageBitmap(thumbnail);
	        	String pic1 = SaveImage(thumbnail);
	        	Change.path1 = pic1;
	        	MainActivity.prefs.edit().putString("picture1", "test").commit();
		       	MainActivity.prefs.edit().putString("picture1", pic1.toString()).commit();
	        	
	           Change.check =0;
	           }
	    	}
	    	if (requestCode == 11) {

				if (resultCode == RESULT_OK) {
					//tvStatus.setText(intent.getStringExtra("SCAN_RESULT_FORMAT"));
					Toast.makeText(Change.this, "Scan Completed.", 5000).show();
					serialnoedit.setText(data.getStringExtra("SCAN_RESULT"));
				} else if (resultCode == RESULT_CANCELED) {
					Toast.makeText(Change.this, "Scan Cancelled.", 5000).show();
				}
			}
			else if (requestCode == 22) {

				if (resultCode == RESULT_OK) {
					//tvStatus.setText(intent.getStringExtra("SCAN_RESULT_FORMAT"));
					Toast.makeText(Change.this, "Scan Completed.", 5000).show();
					partnoedit.setText(data.getStringExtra("SCAN_RESULT"));
				} else if (resultCode == RESULT_CANCELED) {
					Toast.makeText(Change.this, "Scan Cancelled.", 5000).show();
				}
			}

	
	    }
	    	/*if(check==2 || Change.check ==2){
		        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
		        
	        	picture2.setImageBitmap(thumbnail);
	        	SaveImage(thumbnail);
	        	Change.check =0; }
	    	if(check==1 || Change.check ==1){
		        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
		        
	        	picture1.setImageBitmap(thumbnail);
	           Change.check =0; 
	           SaveImage(thumbnail);   
	    	}*/
	    	
		   // thumbnail.recycle();
	       
	    }
	  /*  else if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK && check ==2) {
	       // Uri imagePath = getImageUri();

	        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
	        picture2.setImageBitmap(thumbnail);
	        check =0;
	    }
	    else if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK && check ==3) {
	        
	        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
	        picture3.setImageBitmap(thumbnail);
	        check =0;
	    }
	*/
	
	private String SaveImage(Bitmap finalBitmap) {

	    String root = Environment.getExternalStorageDirectory().toString();
	    File myDir = new File(root + "/ChangeLog/"+MainActivity.prefs.getString("desc",""));
	    myDir.mkdirs();
	    Random generator = new Random();
	    int n = 10000;
	    n = generator.nextInt(n);
	    String fname = "Image-"+ n +".jpg";
	    File file = new File (myDir, fname);
	    if (file.exists ()) file.delete (); 
	    try {
	           FileOutputStream out = new FileOutputStream(file);
	           finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
	           out.flush();
	           out.close();

	    } catch (Exception e) {
	           e.printStackTrace();
	    }
	    return file.getAbsoluteFile().toString();
	}
	private Uri getImageUri() {
	    // Store image in dcim
		Random r = new Random();
		int a = r.nextInt()%100;
	    File file = new File(Environment.getExternalStorageDirectory() + "/sample", "Sample"+a);
	    Uri imgUri = Uri.fromFile(file);

	    return imgUri;
	}
}
